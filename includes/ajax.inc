<?php

/**
 * @file
 * Handles the server side AJAX interactions of MY Pages.
 */

/**
 * Callback AJAX function.
 *
 * @param $year_month
 * @param null $mode
 *
 * @return array
 */
function my_pages_ajax_get_ajax($year_month, $mode = NULL) {
  if ($mode != 'ajax') {
    drupal_set_message('Enable Javascript');
    drupal_goto(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '<front>');
  }

  $month = strtotime($year_month);
  $class = date('F', $month);

  $commands[] = ajax_command_remove(".my-list");
  $commands[] = ajax_command_append("#$class", last_news_block($year_month));

  return array(
    '#type'     => 'ajax',
    '#commands' => $commands,
  );
}