<?php

/**
 * @file
 * Provides the My Pages' administrative interface.
 */

/**
 * Content for the page 'Main news pages'.
 *
 * Content for the the page 'Main my pages', used in the function my_pages_menu()
 * in the item 'Main news pages' in the 'page callback'.
 *
 * @return string|void.
 */
function main_my_pages_content() {
  $header = array(
    array('data' => t('Title'), 'field' => 'title', 'sort' => 'ASC'),
    array('data' => t('Date'), 'field' => 'created', 'sort' => 'ASC'),
    array('data' => t('Status')),
    array('data' => t('Subtitle')),
    array('data' => t('Descriptions')),
    array('data' => t('Actions')),
  );

  $select = db_select('node', 'n')
    ->condition('n.type', 'my_pages')
    ->fields('n')
    //->orderBy('created', 'DESC')
    ->extend('PagerDefault')
    ->extend('TableSort')
    ->limit(4);

  $my_pages = $select
    ->orderByHeader($header)
    ->execute()
    ->fetchAll();

  $row = array();
  if ($my_pages) {
    foreach ($my_pages as $my_page) {
      $my_node = node_load($my_page->nid);

      if ($my_node) {
        $actions = array(
          l(t('edit'), 'admin/settings/main_news_pages/'
                       . $my_node->nid . '/edit'),
          l(t('delete'), 'admin/settings/main_news_pages/'
                         . $my_node->nid . '/delete'),
        );
        $sub     = field_get_items('node', $my_node, 'sub_title');
        $desc    = field_get_items('node', $my_node, 'news_description');
        $row []  = array(
          array('data' => l($my_page->title, 'my_pages/' . $my_page->nid . '/item')),
          array('data' => date('Y-m-d', $my_page->created)),
          array('data' => $my_page->status),
          array('data' => field_view_value('node', $my_node, 'sub_title', $sub[0])),
          array('data' => field_view_value('node', $my_node, 'news_description', $desc[0])),
          array('data' => implode(' | ', $actions)),
        );
      }
    }
  }

  $output = theme('table', array(
      'header' => $header,
      'rows'   => $row,
    )
  );
  $output .= theme('pager');
  return $output;
}

/**
 * Form for the page 'Add News' and 'Edit News'.
 *
 * Form for the the page 'Add News' and 'Edit News', used in the function my_pages_menu()
 * in the item 'Add News' and 'Edit News' in the 'page arguments'.
 *
 * @param $form
 *
 * @param $form_satate
 *
 * @param null $my_pages
 *
 * @return mixed
 */
function my_pages_form($form, &$form_state, $my_pages = NULL) {
  $form['title'] = array(
    '#title'         => t('News name.'),
    '#description'   => t('Insert news name'),
    '#type'          => 'textfield',
    '#default_value' => $my_pages ? $my_pages['title'] : '',
    '#required'      => TRUE,
  );

  $form['sub_title'] = array(
    '#title'         => t('News sub title.'),
    '#description'   => t('Insert news sub title'),
    '#type'          => 'textfield',
    '#default_value' => $my_pages ? $my_pages['sub_title'] : '',
    '#required'      => TRUE,
  );

  $form['news_description'] = array(
    '#title'         => t('News description.'),
    '#description'   => t('Insert news description max length 200'),
    '#type'          => 'textarea',
    '#default_value' => $my_pages ? $my_pages['news_description'] : '',
    '#rows'          => 5,
    '#maxlength'     => 200,
    '#size'          => 30,
    '#required'      => TRUE,
  );

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => $my_pages ? t('Save') : t('Add'),
  );

  if ($my_pages) {
    $form['id'] = array(
      '#type'  => 'value',
      '#value' => $my_pages['nid'],
    );
  }

  return $form;
}

/**
 * Implements hook_form_submit().
 */
function my_pages_form_submit($form, &$form_state) {
  global $user;
  global $language;
  $news = array(
    'title'            => $form_state['values']['title'],
    'sub_title'        => $form_state['values']['sub_title'],
    'news_description' => $form_state['values']['news_description'],
    'created'          => time(),
    'type'             => 'my_pages',
    'status'           => 1,
  );

  if (isset($form_state['values']['id'])) {
    $news['nid']                               = $form_state['values']['id'];
    $node                                      = node_load($news['nid']);
    $node->created                             = $news['created'];
    $node->title                               = $news['title'];
    $node->sub_title['und'][0]['value']        = $news['sub_title'];
    $node->news_description['und'][0]['value'] = $news['news_description'];

    node_save($node);
    drupal_set_message(t('News Saved!'));
  }
  else {
    $node                                      = new stdClass();
    $node->type                                = $news['type'];
    $node->created                             = $news['created'];
    $node->status                              = $news['status'];
    $node->title                               = $news['title'];
    $node->sub_title['und'][0]['value']        = $news['sub_title'];
    $node->news_description['und'][0]['value'] = $news['news_description'];
    $node->language                            = $language->language;
    $node->uid                                 = $user->uid;

    node_save($node);
    drupal_set_message(t('News Added!'));
  }
  drupal_goto('admin/settings/main_news_pages');
}


/**
 * Delete my_pages.
 *
 * Used in the function my_pages_menu().
 * in the item 'Delete News' in the 'page callback'.
 *
 * @param $my_pages
 *
 */
function my_pages_delete($my_pages) {
  $my_p_deleted = db_delete('node')
    ->condition('nid', $my_pages)
    ->execute();
  drupal_set_message(t('News deleted!'));
  drupal_goto('admin/settings/main_news_pages');
}