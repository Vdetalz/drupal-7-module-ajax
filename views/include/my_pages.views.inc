<?php

/**
 * @file
 * Provides the My Pages' frond-end pages.
 */

/**
 * Selects from the database the published content "my_pages".
 *
 * @param $display
 *
 * @return mixed
 */
function news_content($display) {
  $query = db_select('node', 'n')
    ->condition('n.type', 'my_pages')
    ->condition('status', 1)
    ->fields('n')
    ->orderBy('created', 'DESC');

  if ($display == 'block') {
    $limit = variable_get('counts_news', 5);
    $query->range(0, $limit);
  }

  return $query->execute();
}

/**
 * The callback function for the page with the list of the content type "my_pages".
 * Is called in hook_menu().
 *
 * @return string|void
 */
function _my_pages_archive() {
  drupal_set_title(t('News'));

  $items  = array();
  $result = news_content('page')->fetchAll();

  foreach ($result as $node) {
    $items[] = array(
      'data'  => l($node->title, 'node/' . $node->nid),
      'title' => $node->title,
    );
  }

  if (!$result) {
    $page_array['my_pages_arguments'] = array(
      '#title'  => t('News page'),
      '#markup' => t('No News available'),
    );

    return $page_array;
  }
  else {
    return $page_array = theme('my_pages_archive', array('items' => $items));
  }
}

/**
 * The callback function for the single page of the content type "my_pages".
 *
 * @param $my_pages
 *
 * @return string|void
 */
function my_pages_single($my_pages) {
  drupal_set_title($my_pages['title']);
  $item = load_node_information($my_pages);

  if (!$item) {
    $page_array['my_pages_arguments'] = array(
      '#title'  => t('News page'),
      '#markup' => t('No News available'),
    );
    return $page_array;
  }
  else {
    return $page_array = theme('my_pages_single', array('items' => $my_pages));
  }
}