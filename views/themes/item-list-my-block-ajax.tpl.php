<?php

/**
 * @file
 * Displays a list of news in the News Archive Block.
 *
 * Available variables:
 * - $items: An array of news to display.
 */
?>

<ul class="my-list">
  <?php foreach ($items as $item): ?>
    <li><?php echo $item['data']; ?></li>
    <br>
  <?php endforeach; ?>
</ul>
