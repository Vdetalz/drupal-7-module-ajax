<?php

/**
 * @file
 * Displays a list of news in the News Pages Block.
 *
 * Available variables:
 * - $items: An array of news to display.
 */
?>

<?php foreach ($items as $item): ?>
  <span class="title"><?php echo $item['data']; ?></span>
  <br>
  <p><?php echo $item['sub_title']; ?></p>
  <p><?php echo $item['news_description']; ?></p>
  <hr><br><br>
<?php endforeach; ?>