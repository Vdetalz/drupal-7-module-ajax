<?php
/**
 * @file
 * Displays a single page.
 *
 * Available variables:
 * - $items: Array with properties news to display.
 */
?>

<p><?php echo $items['sub_title']; ?></p>
<p><?php echo $items['news_description']; ?></p>
