<?php

/**
 * @file
 * Displays the archive page.
 *
 * Available variables:
 * - $items: An array of news to display.
 */
?>

<?php foreach ($items as $item): ?>
  <span class="title"><?php echo $item['data']; ?></span>
  <br>
  <hr><br><br>
<?php endforeach; ?>