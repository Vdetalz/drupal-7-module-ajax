<?php

/**
 * @file
 * Displays a list of months of news in the News Archive Block.
 *
 * Available variables:
 * - $items: An array of news to display.
 */
?>

<div class="my-list-date">
  <?php foreach ($items as $item): ?>
    <div id="<?php echo $item['date'] ?>">
      <p class="group-date"><?php echo $item['data']; ?></p>
    </div>
    <hr>
    <br>
  <?php endforeach; ?>
</div>
